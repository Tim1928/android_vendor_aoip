$(call inherit-product, device/samsung/hercules/full_hercules.mk)

# Inherit some common CM stuff.
$(call inherit-product, vendor/aoip/configs/gsm.mk)

# Inherit some common CM stuff.
$(call inherit-product, vendor/aoip/configs/common.mk)

PRODUCT_BUILD_PROP_OVERRIDES += PRODUCT_NAME=SGH-T989 TARGET_DEVICE=SGH-T989 BUILD_FINGERPRINT="samsung/SGH-T989/SGH-T989:4.1.2/JZO54K/T989UVMC6:user/release-keys" PRIVATE_BUILD_DESC="SGH-T989-user 4.1.2 IMM76D UVMC6 release-keys"

PRODUCT_NAME := hercules
PRODUCT_DEVICE := hercules

# Optional packages
PRODUCT_PACKAGES += \
    DashClock

# Copy device specific prebuilt files.
PRODUCT_COPY_FILES += \
    vendor/aoip/prebuilt/bootanimations/BOOTANIMATION-480x800.zip:system/media/bootanimation.zip
